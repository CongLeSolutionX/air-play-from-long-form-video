# Integrating AirPlay for Long-Form Video Apps
Integrate AirPlay features and implement a dedicated external playback experience by preparing the routing system for long-form video playback. 

## Overview

- Note: This sample code project is associated with WWDC 2019 session [507: Best Practices for 3rd Party AirPlay](https://developer.apple.com/videos/play/wwdc19/507/).